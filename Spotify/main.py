import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk
import pygame
import os
import threading
import time
from moviepy.editor import VideoFileClip
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg


class Spotify:
    def __init__(self, master):
        self.master = master
        master.title("Spotify")
        master.geometry("1570x800")
        master.configure(bg="black")
        master.resizable(False, False)
        master.attributes('-toolwindow', True) 
        
        self.welcome_label = tk.Label(self.master, text="🎹🎬🎤🎧🎼👨‍🎤 Welcome in the Musical World !!! 👨‍🎤🎼🎧🎤🎬🎹", font=("Comic Sans MS", 30, "bold"),bg="black", fg="silver")
        self.welcome_label.grid(row=0, column=0, padx=50, pady=5)
        
        self.music_player_state = "stopped"
        
        self.create_buttons1()
        self.create_buttons2()
        self.create_song_list()
        self.create_advertising_slide()
        self.create_image_frame()
        self.create_time_slider()
        self.update_music_time()
    
        pygame.mixer.init()

    def create_buttons1(self):
        button_images = [
            "Spotify/singers/neha.jpg",
            "Spotify/singers/asha.jpg",
            "Spotify/singers/arjit.jpg",
            "Spotify/singers/darshan.jpg",
        ]
        
        data=500
        for i, image_path in enumerate(button_images):
            image = Image.open(image_path)
            image = image.resize((120, 120))
            photo = ImageTk.PhotoImage(image)
            button = tk.Button(self.master, image=photo, bd=0, bg="black")
            button.image = photo
            button.place(x=data, y=400)
            button.bind("<Button-1>", lambda event, i=i: self.show_song_list(i+1))
            data=data+150
            
    def create_buttons2(self):
        button_images = [
            "Spotify/singers/alka.jpg",
            "Spotify/singers/sonu.jpg",
            "Spotify/singers/abc.jpg",
            "Spotify/singers/shreya.jpg",
        ]
        
        data=500
        for i, image_path in enumerate(button_images):
            image = Image.open(image_path)
            image = image.resize((120, 120))
            photo = ImageTk.PhotoImage(image)
            button = tk.Button(self.master, image=photo, bd=0, bg="black")
            button.image = photo
            button.place(x=data, y=550)
            button.bind("<Button-1>", lambda event, i=i+4: self.show_song_list(i+1))
            data=data+150
            
    def create_song_list(self):
        self.song_listbox = tk.Listbox(self.master, bd=0, bg="black", fg="white", font=("Arial", 12,"bold"), justify="left",selectbackground="gray30",activestyle="none")
        self.song_listbox.place(x=50, y=75, height=600, width=400)
        
        self.song_listbox.bind("<<ListboxSelect>>", self.on_select_song)
        self.song_listbox.insert(tk.END, "")
        self.song_listbox.insert(tk.END, "")
        self.song_listbox.insert(tk.END, "")
        self.song_listbox.insert(tk.END, "")
        self.song_listbox.insert(tk.END, "")
        self.song_listbox.insert(tk.END, "")
        self.song_listbox.insert(tk.END, "")
        self.song_listbox.insert(tk.END, "")
        self.song_listbox.insert(tk.END, "")
        self.song_listbox.insert(tk.END, "         “Where words fail, music speaks.”")
        self.song_listbox.insert(tk.END, "                — Hans Christian Andersen.")
       
    def show_song_list(self, button_number):
        self.song_listbox.delete(0, tk.END)
        
        if button_number == 1:
           songs = ["1 Sawan Mein Lag Gayi Aag","2 Kala Chashma","3 Ghungta","4 Gaadi Kaali","5 Odhani","6 Daaru Peeke Dance","7 La La La","8 Mehbooba","9 Mud Mud Ke","10 Shona Shona","11 Do Gallan","12 Khyaal Rakhya Kar","13 Sorry Song","14 Nehu Da Vyah","15 Mile Ho Tum Humko","16 Badri ki Dulhania","17 Gali Gali","18 Main Tera Boyfriend","19 Akkad Bakkad","20 Dilbar","21 Garmi","22 Chamma Chamma","23 Kar Gayi Chull","24 Jamna Paar","25 Aankh Marey","26 Aao Raja","27 London Thumakda","28 Coca Cola","29 Tu Hi Yaar Mera","30 Dil Ko Karaar Aaya"]
        elif button_number == 2:
            songs = ["1 In Aankhon Ki Masti","2 Parde Mein Rahne Do","3 Aaiye Meharban","4 Kehdoon Tumhen","5 Mausam Mastana","6 Aao Na Gale Laga Lo Na","7 Hungama Ho Gaya","8 Radha Kaise Na Jale","9 Pyar Karne Wale","10 Khali Hath Sham Aai Hai","11 Dillagi Ne Di Hawa","12 Tere Ishq Ka Mujh Pe Hua","13 Aao Huzoor Tum Ko","14 Chehra Kya Dekhte Ho","15 Teri Yaad Aa Rahi Hai","16 Jambhul Pikalya Zadakhali","17 Gomu Sangtina Mazya","18 Mee Raat Takali","19 Aala Aala Vara","20 Chandra Aahe Sakshiila","21 Choricha Mamla","22 Nabh Utaru Aala","22 Dhundi Kalyana","23 Ushakaal Hota Hota","24 Ka Re Durawa Ka Re Abola","25 Khel Kunala Daivacha Kalala","26 Jagnyachi Hi Majja","27 Saanj Ye Gokuli","28 Dhundit Gau Mastit Raahu ","29 Mee Maj Harpun Basale Ge","30 Hi Vaat Door Jaate"]
        elif button_number == 3:
            songs = ["1 Phir Bhi Tumko Chaahunga","2 Agar Tum Saath Ho","3 Satranga","4 Soch Na Sake","5 Dhokha","6 PHIR KABHI","7 Tere Pyaar Mein","8 Kalank","9 Humdard","10 O Bedardeya","11 Muskurane","12 Jhoome Jo Pathaan","13 Tum Kya Mile","14 Ae Dil Hai Mushkil ","15 Hum Mar Jayenge","16 O Maahi","17 Hamari Adhuri Kahani","18 Lutt Putt Gaya","19 Chaleya","20 Tera Yaar Hoon Main","21 Desh Mere","22 Galti Se Mistake","23 Heeriye","24 Nashe Si Chadh Gayi","25 Apna Bana Le","26 Lambiyaan Si Judaiyaan","27 Woh Din","28 Zaalima","29 Lehra Do","30 Naina"]
        elif button_number == 4:
            songs = ["1 Suraj Hua Maddham","2 Main Agar Kahoon","3 Abhi Mujh Mein Kahin","4  Do Pal","5 Pyar Ki Ek Kahani","6 Dil Dooba","7 Yeh Dil Deewana","8 Deewana Tera","9 Mere Hath Mein","10 Tanhayee","11 Shukran Allah","12 Main Hoon Na","13 Ab Mujhe Aate Hai","14 Satrangi Re","15 Zoobi Doobi","16 Sandese Aate Hai","17 Papa Meri Jaan","18 Papa Mere Papa","19 Bole Chudiyan","20 Mujhse Shaadi Karogi","21 Neeche Phoolon Ki Dukan","22 Mere Yaar Ki Shaadi Hai","23 Bhagwaan Hai Kahan Re Tu","24 Mera Yaar Dildaar","25 Main Duniya Teri Chhod Chala","26 Pyar Dilon Ka Mela Hai","27 Diwana Hai Ye Man","28 Har Ek Friend Kamina Hota Hai","29 Jaane Nahin Denge","30 Kabhi Khushi Kabhie Gham"]
        elif button_number == 5:
            songs = ["1 Chura Ke Dil Mera","2 Ae Mere Humsafar","3 Baazigar O Baazigar","4 Ladki Badi Anjani Hai","5 Neend Churayee Meri","6 Aisi Deewangi","7 Dil ne yeh kaha hain dil se","8 Tip Tip Barsa Paani","9 Ek Din Aap","10 Shona Shona","11 Dilbar Dilbar (Jhankar)","12 Odh Li Chunariya Tere Na","13 Taal Se Taal","14 Raja KO Rani Se","15 Tumse Milna","16 Aankh Teri Chhalke To","17 Rab Kare","18 Teri chunariya","19 Sajan Sajan Teri Dulhan","20 Yeh Bandhan Toh","21 Udja Kale Kawan ","22 Dil Ke Badle Sanam","23 Ishq Hai Zindagi","24 Tum Toh Dhokhebaaz Ho","25 Dil Laga Liya Maine","26 Kitna Pyaara Tujhe Rabne Banaya","27 Shaam Bhi Khoob Hai","28 Allah Allah","29 Tinak Tin Tana","30 Gore Gore Mukhde Pe"]
        elif button_number == 6:
            songs = ["1 Mera Dil Bhi Kitna Pagal Ha","2 Baazigar O Baazigar","3 Chura Ke Dil Mera","4 Sochenge Tumhe Pyar","5 Jeeye To Jeeye Kaise","6 Ladki Badi Anjani Hai","7 Neend Churayee Meri","8 O Lal Dupatte Wali","9 Dard Karaara","10 Shona Shona","11 Tumse Milne Ko Dil","12 Jadoo Hai Tera","13 Tujhe Dekha To","14 Dil Mera Churaya Kyun","15 Teri Umeed Tera Intezar","16 Teri chunariya","17 Saajanji Ghar Aaye","18 Hum Teri Mohabbat Me","19 Woh Ladki Bahut Yaad Aati","20 Tumse Milne Ko Dil","21 Kore Kore Sapne","22 Jaati Hoon Main ","23 Milne Ki Tum Koshish Karna","24 Mohabbat Dil Ka Sakoon","25 Apni Bhi Zindagi Mein","26 Pucho Zara Pucho","27 Tere Ishq Mein Naachenge","28 Dil Jaane Jigar Tujh Pe","29 O Jaane Jaan","30 Dheere Dheere Se"]
        elif button_number == 7:
            songs = ["1  Dil Se Re","2 Muqabala Muqabala","3 Ek Ho Gaye Hum Aur Tum ","4 Rangeela Re","5  Mitwa","6  Urvashi Urvashi","7 O Humdum Suniyo Re","8  Jai Ho","9 Mud Mud Ke","10 Tere Bina","11 Ek Ho Gaye Hum Aur Tum","12 Saajan Aayo Re","13  Kay Sera Sera","14 Ek Bagiya Mein","15 Rang De","16 Maa Tujhe Salaam","17 Masakali 2.0","18 Luka Chuppi","19 Kun Faya Kun","20 Tu Hai","21 Jashn-E-Bahaaraa","22 O Paalanhaare","23 Ponni Nadhi","24 Radha Kaise Na Jale","25 Raanjhanaa","26 Param Sundari","27 Latoo","28 Yeh Haseen Vadiyan Yeh Khula Aasaman","29 Nee Singam Dhan","30 Ranvijay’s Entry Medley"]
        elif button_number == 8:
            songs = ["1 Sunn Raha Hai","2 Samjhawan","3 Saibo","4 Saans","5 Kasto Mazza","6 Tu","7 Tum Kya Mile","8 Jugraafiya","9 Jab Saiyaan","10 Yeh Aaina","11 Bolo Na","12 Khyaal Rakhya Kar","13 Noor E Khuda","14 Aashiyan","15 Chikni Chameli","16 Zihaal e Miskin","17 Do Anjaane Ajnabi","18 Saathiyaa","19 Nannare","20 Aashiyan","21 Baharla Ha Madhumas","22 God Allah Aur Bhagwan","23 Jeene Laga Hoon","24 Hum Tumko Nigahon Mein","25 Sukh Kalale","26 Kar Har Maidaan Fateh","27 Teri Ore","28 Ghar More Pardesiya","29 Mere Dholna","30 Maheroo Maheroo"]
            
        for song in songs:
            self.song_listbox.insert(tk.END, f"{song}")
       

    def create_advertising_slide(self):
        canvas = tk.Canvas(self.master, bg="black", bd=0, highlightthickness=0)
        canvas.place(x=500, y=80, relwidth=700, height=300)  

        ad_images = [
            "Spotify/advertise/ad1.jpg",
           "Spotify/advertise/ad2.jpg",
           "Spotify/advertise/ad3.jpg",
           "Spotify/advertise/ad4.jpg",
           "Spotify/advertise/ad5.jpg",
           "Spotify/advertise/ad6.jpg",
           "Spotify/advertise/ad7.jpg"
        ]
        
        self.ad_photo_images = []
        for image_path in ad_images:
            image = Image.open(image_path)
            image = image.resize((570, 250))  
            photo = ImageTk.PhotoImage(image)
            self.ad_photo_images.append(photo)

        self.ad_index = 0
        self.ad_label = canvas.create_image(0, 0, anchor=tk.NW, image=self.ad_photo_images[self.ad_index])
        self.create_image_indicator(canvas)

        self.animate_advertising_slide(canvas)

    def create_image_indicator(self, canvas):
        total_images = len(self.ad_photo_images)
        x_start = (323 - 10 * total_images) 
        y = 260  
        self.indicators = []
        for i in range(total_images):
            circle = canvas.create_oval(x_start + i * 10, y, x_start + (i + 1) * 10, y + 10, fill="white")
            self.indicators.append(circle)

    def update_indicator(self, canvas, index):
        for i, indicator in enumerate(self.indicators):
            canvas.itemconfig(indicator, fill="white" if i == index else "darkgray")

    def animate_advertising_slide(self, canvas):
        canvas.after(3000, lambda: self.next_advertisement(canvas))

    def next_advertisement(self, canvas):
        canvas.itemconfig(self.ad_label, image=self.ad_photo_images[(self.ad_index + 1) % len(self.ad_photo_images)])
        self.update_indicator(canvas, (self.ad_index + 1) % len(self.ad_photo_images))
        self.ad_index = (self.ad_index + 1) % len(self.ad_photo_images)
        canvas.after(3000, lambda: self.next_advertisement(canvas))

    def create_image_frame(self):
        self.image_frame = tk.Frame(self.master, bg="black", bd=1, relief=tk.RIDGE)
        self.image_frame.place(x=1120, y=75, height=600, width=400)

        self.image_label = tk.Label(self.image_frame, bg="black")
        self.image_label.pack()
        

    def on_select_song(self, event):
        selection = event.widget.curselection()
        if selection:
            index = selection[0]
            song = event.widget.get(index)
            self.display_song_image(song)

    def display_song_image(self, song):
        image_path = "Spotify/advertise/display.jpg"  
        
        if song == "1 Sawan Mein Lag Gayi Aag":
            image_path = "Spotify/song_images/song1.jpg"
        elif song == "2 Kala Chashma":
            image_path = "Spotify/song_images/song2.jpeg"
        elif song == "3 Ghungta":
            image_path = "Spotify/song_images/song3.jpeg"
        elif song == "4 Gaadi Kaali":
            image_path = "Spotify/song_images/song4.jpg"
        elif song == "5 Odhani":
            image_path = "Spotify/song_images/song5.jpeg"
        elif song == "6 Daaru Peeke Dance":
            image_path = "Spotify/song_images/song6.jpg"
        elif song == "7 La La La":
            image_path = "Spotify/song_images/song7.jpg"
        elif song == "8 Mehbooba":
            image_path = "Spotify/song_images/song8.jpg"
        elif song == "9 Mud Mud Ke":
            image_path ="Spotify/song_images/song9.jpg"
        elif song == "10 Shona Shona":
            image_path = "Spotify/song_images/song10.jpg"
        elif song == "11 Do Gallan":
            image_path ="Spotify/song_images/song11.jpg"
        elif song == "12 Khyaal Rakhya Kar":
            image_path = "Spotify/song_images/song12.jpg"
        elif song == "13 Sorry Song":
            image_path = "Spotify/song_images/song13.jpg"
        elif song == "14 Nehu Da Vyah":
            image_path ="Spotify/song_images/song14.jpg"
        elif song == "15 Mile Ho Tum Humko":
            image_path = "Spotify/song_images/song15.jpg"
            
        elif song == "1 In Aankhon Ki Masti":
            image_path = "Spotify/song_images/song16.jpg"
        elif song == "2 Parde Mein Rahne Do":
            image_path = "Spotify/song_images/song17.jpg"
        elif song == "3 Aaiye Meharban":
            image_path = "Spotify/song_images/song18.jpg"
        elif song == "4 Kehdoon Tumhen":
            image_path = "Spotify/song_images/song19.jpg"
        elif song == "5 Mausam Mastana":
            image_path = "Spotify/song_images/song20.jpg"
        elif song == "6 Aao Na, Gale Laga Lo Na":
            image_path = "Spotify/song_images/song21.jpg"
        elif song == "7 Hungama Ho Gaya":
            image_path = "Spotify/song_images/song22.jpg"
        elif song == "8 Radha Kaise Na Jale":
            image_path = "Spotify/song_images/song23.jpg"
        elif song == "9 Pyar Karne Wale":
            image_path ="Spotify/song_images/song24.jpg"
        elif song == "10 Khali Hath Sham Aai Hai":
            image_path = "Spotify/song_images/song25.jpg"
        elif song == "11 Dillagi Ne Di Hawa":
            image_path ="Spotify/song_images/song26.jpg"
        elif song == "12 Tere Ishq Ka Mujh Pe Hua":
            image_path = "Spotify/song_images/song27.jpg"
        elif song == "13 Aao Huzoor Tum Ko":
            image_path = "Spotify/song_images/song28.jpg"
        elif song == "14 Chehra Kya Dekhte Ho":
            image_path ="Spotify/song_images/song29.jpg"
        elif song == "15 Teri Yaad Aa Rahi Hai":
            image_path = "Spotify/song_images/song30.jpg"
            
        elif song == "1 Phir Bhi Tumko Chaahunga":
            image_path = "Spotify/song_images/song31.jpg"
        elif song == "2 Agar Tum Saath Ho":
            image_path = "Spotify/song_images/song32.jpg"
        elif song == "3 Satranga":
            image_path = "Spotify/song_images/song33.jpg"
        elif song == "4 Soch Na Sake":
            image_path = "Spotify/song_images/song34.jpg"
        elif song == "5 Dhokha":
            image_path = "Spotify/song_images/song35.jpg"
        elif song == "6 PHIR KABHI":
            image_path = "Spotify/song_images/song36.jpg"
        elif song == "7 Tere Pyaar Mein":
            image_path = "Spotify/song_images/song37.jpg"
        elif song == "8 Kalank":
            image_path = "Spotify/song_images/song38.jpg"
        elif song == "9 Humdard":
            image_path ="Spotify/song_images/song39.jpg"
        elif song == "10 O Bedardeya":
            image_path = "Spotify/song_images/song40.jpg"
        elif song == "11 Muskurane":
            image_path ="Spotify/song_images/song41.jpg"
        elif song == "12 Jhoome Jo Pathaan":
            image_path = "Spotify/song_images/song42.jpg"
        elif song == "13 Tum Kya Mile":
            image_path = "Spotify/song_images/song43.jpg"
        elif song == "14 Ae Dil Hai Mushkil ":
            image_path ="Spotify/song_images/song44.jpg"
        elif song == "15 Hum Mar Jayenge":
            image_path = "Spotify/song_images/song45.jpg"
        
        elif song == "1 Suraj Hua Maddham":
            image_path = "Spotify/song_images/song46.jpg"
        elif song == "2 Main Agar Kahoon":
            image_path = "Spotify/song_images/song47.jpg"
        elif song == "3 Abhi Mujh Mein Kahin":
            image_path = "Spotify/song_images/song48.jpg"
        elif song == "4  Do Pal":
            image_path = "Spotify/song_images/song49.jpg"
        elif song == "5 Pyar Ki Ek Kahani":
            image_path = "Spotify/song_images/song50.jpg"
        elif song == "6 Dil Dooba":
            image_path = "Spotify/song_images/song51.jpg"
        elif song == "7 Yeh Dil Deewana":
            image_path = "Spotify/song_images/song52.jpg"
        elif song == "8 Deewana Tera":
            image_path = "Spotify/song_images/song53.jpg"
        elif song == "9 Mere Hath Mein":
            image_path ="Spotify/song_images/song54.jpg"
        elif song == "10 Tanhayee":
            image_path = "Spotify/song_images/song55.jpg"
        elif song == "11 Shukran Allah":
            image_path ="Spotify/song_images/song56.jpg"
        elif song == "12 Main Hoon Na":
            image_path = "Spotify/song_images/song57.jpg"
        elif song == "13 Ab Mujhe Aate Hai":
            image_path = "Spotify/song_images/song58.jpg"
        elif song == "14 Satrangi Re":
            image_path ="Spotify/song_images/song59.jpg"
        elif song == "15 Zoobi Doobi":
            image_path = "Spotify/song_images/song60.jpg"
        
        elif song == "1 Chura Ke Dil Mera":
            image_path = "Spotify/song_images/song61.jpg"
        elif song == "2 Ae Mere Humsafar":
            image_path = "Spotify/song_images/song62.jpg"
        elif song == "3 Baazigar O Baazigar":
            image_path = "Spotify/song_images/song63.jpg"
        elif song == "4 Ladki Badi Anjani Hai":
            image_path = "Spotify/song_images/song64.jpg"
        elif song == "5 Neend Churayee Meri":
            image_path = "Spotify/song_images/song65.jpg"
        elif song == "6 Aisi Deewangi":
            image_path = "Spotify/song_images/song66.jpg"
        elif song == "7 Dil ne yeh kaha hain dil se":
            image_path = "Spotify/song_images/song67.jpg"
        elif song == "8 Tip Tip Barsa Paani":
            image_path = "Spotify/song_images/song68.jpg"
        elif song == "9 Ek Din Aap":
            image_path ="Spotify/song_images/song69.jpg"
        elif song == "10 Shona Shona":
            image_path = "Spotify/song_images/song70.jpg"
        elif song == "11 Dilbar Dilbar (Jhankar)":
            image_path ="Spotify/song_images/song71.jpg"
        elif song == "12 Odh Li Chunariya Tere Na":
            image_path = "Spotify/song_images/song72.jpg"
        elif song == "13 Taal Se Taal":
            image_path = "Spotify/song_images/song73.jpg"
        elif song == "14 Raja KO Rani Se":
            image_path ="Spotify/song_images/song74.jpg"
        elif song == "15 Tumse Milna":
            image_path = "Spotify/song_images/song75.jpg"
        
        elif song == "1 Mera Dil Bhi Kitna Pagal Ha":
            image_path = "Spotify/song_images/song76.jpg"
        elif song == "2 Baazigar O Baazigar":
            image_path = "Spotify/song_images/song77.jpg"
        elif song == "3 Chura Ke Dil Mera":
            image_path = "Spotify/song_images/song78.jpg"
        elif song == "4 Sochenge Tumhe Pyar":
            image_path = "Spotify/song_images/song79.jpg"
        elif song == "5 Jeeye To Jeeye Kaise":
            image_path = "Spotify/song_images/song80.jpg"
        elif song == "6 Ladki Badi Anjani Hai":
            image_path = "Spotify/song_images/song81.jpg"
        elif song == "7 Neend Churayee Meri":
            image_path = "Spotify/song_images/song82.jpg"
        elif song == "8 O Lal Dupatte Wali":
            image_path = "Spotify/song_images/song83.jpg"
        elif song == "9 Dard Karaara":
            image_path ="Spotify/song_images/song84.jpg"
        elif song == "10 Shona Shona":
            image_path = "Spotify/song_images/song85.jpg"
        elif song == "11 Tumse Milne Ko Dil":
            image_path ="Spotify/song_images/song86.jpg"
        elif song == "12 Jadoo Hai Tera":
            image_path = "Spotify/song_images/song87.jpg"
        elif song == "13 Tujhe Dekha To":
            image_path = "Spotify/song_images/song88.jpg"
        elif song == "14 Dil Mera Churaya Kyun":
            image_path ="Spotify/song_images/song89.jpg"
        elif song == "15 Teri Umeed Tera Intezar":
            image_path = "Spotify/song_images/song90.jpg"
            
        elif song == "1  Dil Se Re":
            image_path = "Spotify/song_images/song91.jpg"
        elif song == "2 Muqabala Muqabala":
            image_path = "Spotify/song_images/song92.jpg"
        elif song == "3 Ek Ho Gaye Hum Aur Tum ":
            image_path = "Spotify/song_images/song93.jpg"
        elif song == "4 Rangeela Re":
            image_path = "Spotify/song_images/song94.jpg"
        elif song == "5  Mitwa":
            image_path = "Spotify/song_images/song95.jpg"
        elif song == "6  Urvashi Urvashi":
            image_path = "Spotify/song_images/song96.jpg"
        elif song == "7 O Humdum Suniyo Re":
            image_path = "Spotify/song_images/song97.jpg"
        elif song == "8  Jai Ho":
            image_path = "Spotify/song_images/song98.jpg"
        elif song == "9 Mud Mud Ke":
            image_path ="Spotify/song_images/song99.jpg"
        elif song == "10 Tere Bina":
            image_path = "Spotify/song_images/song100.jpg"
        elif song == "11 Ek Ho Gaye Hum Aur Tum":
            image_path ="Spotify/song_images/song101.jpg"
        elif song == "12 Saajan Aayo Re":
            image_path = "Spotify/song_images/song102.jpg"
        elif song == "13  Kay Sera Sera":
            image_path = "Spotify/song_images/song103.jpg"
        elif song == "14 Ek Bagiya Mein":
            image_path ="Spotify/song_images/song104.jpg"
        elif song == "15 Rang De":
            image_path = "Spotify/song_images/song105.jpg"
        
        elif song == "1 Sunn Raha Hai":
            image_path = "Spotify/song_images/song106.jpg"
        elif song == "2 Samjhawan":
            image_path = "Spotify/song_images/song107.jpg"
        elif song == "3 Saibo":
            image_path = "Spotify/song_images/song108.jpg"
        elif song == "4 Saans":
            image_path = "Spotify/song_images/song109.jpg"
        elif song == "5 Kasto Mazza":
            image_path = "Spotify/song_images/song110.jpg"
        elif song == "6 Tu":
            image_path = "Spotify/song_images/song111.jpg"
        elif song == "7 Tum Kya Mile":
            image_path = "Spotify/song_images/song112.jpg"
        elif song == "8 Jugraafiya":
            image_path = "Spotify/song_images/song113.jpg"
        elif song == "9 Jab Saiyaan":
            image_path ="Spotify/song_images/song114.jpg"
        elif song == "10 Yeh Aaina":
            image_path = "Spotify/song_images/song115.jpg"
        elif song == "11 Bolo Na":
            image_path ="Spotify/song_images/song116.jpg"
        elif song == "12 Khyaal Rakhya Kar":
            image_path = "Spotify/song_images/song117.jpg"
        elif song == "13 Noor E Khuda":
            image_path = "Spotify/song_images/song118.jpg"
        elif song == "14 Aashiyan":
            image_path ="Spotify/song_images/song119.jpg"
        elif song == "15 Chikni Chameli":
            image_path = "Spotify/song_images/song120.jpg"
            
        

        if image_path:
            image = Image.open(image_path)
            image = image.resize((400, 600))
            photo = ImageTk.PhotoImage(image)
            self.image_label.config(image=photo)
            self.image_label.image = photo
            

    def create_image_frame(self):
        self.image_frame = tk.Frame(self.master, bg="black", bd=1, relief=tk.RIDGE)
        self.image_frame.place(x=1120, y=75, height=600, width=400)

        default_image = Image.open("Spotify/advertise/display.jpg")
        default_image = default_image.resize((400, 600))
        self.default_photo = ImageTk.PhotoImage(default_image)
        self.image_label = tk.Label(self.image_frame, bg="black", image=self.default_photo)
        self.image_label.pack()

    def on_select_song(self, event):
        selection = event.widget.curselection()
        if selection:
            index = selection[0]
            song = event.widget.get(index)
            song_path = self.get_song_path(song)
            self.play_song(song_path)

            
            self.display_song_image(song)
            
            if self.music_player_state == "paused":
                self.play_pause_button.config(image=self.pause_photo, command=self.pause_music)
            else:
                self.play_pause_button.config(image=self.play_photo, command=self.play_music)

           
            threading.Thread(target=self.update_time_slider_thread).start()

    def get_song_path(self, song):
        
        song_paths = {
            "1 Sawan Mein Lag Gayi Aag": "Spotify/Songs/song1.mp3",
            "2 Kala Chashma": "Spotify/Songs/song2.mp3",
            "3 Ghungta":"Spotify/Songs/song3.mp3",
            "4 Gaadi Kaali":"Spotify/Songs/song4.mp3",
            "5 Odhani":"Spotify/Songs/song5.mp3",
            "6 Daaru Peeke Dance":"Spotify/Songs/song6.mp3",
            "7 La La La":"Spotify/Songs/song7.mp3",
            "8 Mehbooba":"Spotify/Songs/song8.mp3",
            "9 Mud Mud Ke":"Spotify/Songs/song9.mp3",
            "10 Shona Shona":"Spotify/Songs/song10.mp3",
            "11 Do Gallan":"Spotify/Songs/song11.mp3",
            "12 Khyaal Rakhya Kar":"Spotify/Songs/song12.mp3",
            "13 Sorry Song":"Spotify/Songs/song13.mp3",
            "14 Nehu Da Vyah":"Spotify/Songs/song14.mp3",
            "15 Mile Ho Tum Humko":"Spotify/Songs/song15.mp3",
            
            "1 In Aankhon Ki Masti":"Spotify/Songs/song16.mp3",
            "2 Parde Mein Rahne Do":"Spotify/Songs/song17.mp3",
            "3 Aaiye Meharban":"Spotify/Songs/song18.mp3",
            "4 Kehdoon Tumhen":"Spotify/Songs/song19.mp3",
            "5 Mausam Mastana":"Spotify/Songs/song20.mp3",
            "6 Aao Na, Gale Laga Lo Na":"Spotify/Songs/song21.mp3",
            "7 Hungama Ho Gaya":"Spotify/Songs/song22.mp3",
            "8 Radha Kaise Na Jale":"Spotify/Songs/song23.mp3",
            "9 Pyar Karne Wale":"Spotify/Songs/song24.mp3",
            "10 Khali Hath Sham Aai Hai":"Spotify/Songs/song25.mp3",
            "11 Dillagi Ne Di Hawa":"Spotify/Songs/song26.mp3",
            "12 Tere Ishq Ka Mujh Pe Hua":"Spotify/Songs/song27.mp3",
            "13 Aao Huzoor Tum Ko":"Spotify/Songs/song28.mp3",
            "14 Chehra Kya Dekhte Ho":"Spotify/Songs/song29.mp3",
            "15 Teri Yaad Aa Rahi Hai":"Spotify/Songs/song30.mp3",
            
            "1 Phir Bhi Tumko Chaahunga":"Spotify/Songs/song31.mp3",
            "2 Agar Tum Saath Ho":"Spotify/Songs/song32.mp3",
            "3 Satranga":"Spotify/Songs/song33.mp3",
            "4 Soch Na Sake":"Spotify/Songs/song34.mp3",
            "5 Dhokha":"Spotify/Songs/song35.mp3",
            "6 PHIR KABHI":"Spotify/Songs/song36.mp3",
            "7 Tere Pyaar Mein":"Spotify/Songs/song37.mp3",
            "8 Kalank":"Spotify/Songs/song38.mp3",
            "9 Humdard":"Spotify/Songs/song39.mp3",
            "10 O Bedardeya":"Spotify/Songs/song40.mp3",
            "11 Muskurane":"Spotify/Songs/song41.mp3",
            "12 Jhoome Jo Pathaan":"Spotify/Songs/song42.mp3",
            "13 Tum Kya Mile":"Spotify/Songs/song43.mp3",
            "14 Ae Dil Hai Mushkil ":"Spotify/Songs/song44.mp3",
            "15 Hum Mar Jayenge":"Spotify/Songs/song45.mp3",
            
            "1 Suraj Hua Maddham":"Spotify/Songs/song46.mp3",
            "2 Main Agar Kahoon":"Spotify/Songs/song47.mp3",
            "3 Abhi Mujh Mein Kahin":"Spotify/Songs/song48.mp3",
            "4  Do Pal":"Spotify/Songs/song49.mp3",
            "5 Pyar Ki Ek Kahani":"Spotify/Songs/song50.mp3",
            "6 Dil Dooba":"Spotify/Songs/song51.mp3",
            "7 Yeh Dil Deewana":"Spotify/Songs/song52.mp3",
            "8 Deewana Tera":"Spotify/Songs/song53.mp3",
            "9 Mere Hath Mein":"Spotify/Songs/song54.mp3",
            "10 Tanhayee":"Spotify/Songs/song55.mp3",
            "11 Shukran Allah":"Spotify/Songs/song56.mp3",
            "12 Main Hoon Na":"Spotify/Songs/song57.mp3",
            "13 Ab Mujhe Aate Hai":"Spotify/Songs/song58.mp3",
            "14 Satrangi Re":"Spotify/Songs/song59.mp3",
            "15 Zoobi Doobi":"Spotify/Songs/song60.mp3",
            
            "1 Chura Ke Dil Mera":"Spotify/Songs/song61.mp3",
            "2 Ae Mere Humsafar":"Spotify/Songs/song62.mp3",
            "3 Baazigar O Baazigar":"Spotify/Songs/song63.mp3",
            "4 Ladki Badi Anjani Hai":"Spotify/Songs/song64.mp3",
            "5 Neend Churayee Meri":"Spotify/Songs/song65.mp3",
            "6 Aisi Deewangi":"Spotify/Songs/song66.mp3",
            "7 Dil ne yeh kaha hain dil se":"Spotify/Songs/song67.mp3",
            "8 Tip Tip Barsa Paani":"Spotify/Songs/song68.mp3",
            "9 Ek Din Aap":"Spotify/Songs/song69.mp3",
            "10 Shona Shona":"Spotify/Songs/song70.mp3",
            "11 Dilbar Dilbar (Jhankar)":"Spotify/Songs/song71.mp3",
            "12 Odh Li Chunariya Tere Na":"Spotify/Songs/song72.mp3",
            "13 Taal Se Taal":"Spotify/Songs/song73.mp3",
            "14 Raja KO Rani Se":"Spotify/Songs/song74.mp3",
            "15 Tumse Milna":"Spotify/Songs/song75.mp3",
            
            "1 Mera Dil Bhi Kitna Pagal Ha":"Spotify/Songs/song76.mp3",
            "2 Baazigar O Baazigar":"Spotify/Songs/song77.mp3",
            "3 Chura Ke Dil Mera":"Spotify/Songs/song78.mp3",
            "4 Sochenge Tumhe Pyar":"Spotify/Songs/song79.mp3",
            "5 Jeeye To Jeeye Kaise":"Spotify/Songs/song80.mp3",
            "6 Ladki Badi Anjani Hai":"Spotify/Songs/song81.mp3",
            "7 Neend Churayee Meri":"Spotify/Songs/song82.mp3",
            "8 O Lal Dupatte Wali":"Spotify/Songs/song83.mp3",
            "9 Dard Karaara":"Spotify/Songs/song84.mp3",
            "10 Shona Shona":"Spotify/Songs/song85.mp3",
            "11 Tumse Milne Ko Dil":"Spotify/Songs/song86.mp3",
            "12 Jadoo Hai Tera":"Spotify/Songs/song87.mp3",
            "13 Tujhe Dekha To":"Spotify/Songs/song88.mp3",
            "14 Dil Mera Churaya Kyun":"Spotify/Songs/song89.mp3",
            "15 Teri Umeed Tera Intezar":"Spotify/Songs/song90.mp3",
            
            "1  Dil Se Re":"Spotify/Songs/song91.mp3",
            "2 Muqabala Muqabala":"Spotify/Songs/song92.mp3",
            "3 Ek Ho Gaye Hum Aur Tum ":"Spotify/Songs/song93.mp3",
            "4 Rangeela Re":"Spotify/Songs/song94.mp3",
            "5  Mitwa":"Spotify/Songs/song95.mp3",
            "6  Urvashi Urvashi":"Spotify/Songs/song96.mp3",
            "7 O Humdum Suniyo Re":"Spotify/Songs/song97.mp3",
            "8  Jai Ho":"Spotify/Songs/song98.mp3",
            "9 Mud Mud Ke":"Spotify/Songs/song99.mp3",
            "10 Tere Bina":"Spotify/Songs/song100.mp3",
            "11 Ek Ho Gaye Hum Aur Tum":"Spotify/Songs/song101.mp3",
            "12 Saajan Aayo Re":"Spotify/Songs/song102.mp3",
            "13  Kay Sera Sera":"Spotify/Songs/song103.mp3",
            "14 Ek Bagiya Mein":"Spotify/Songs/song104.mp3",
            "15 Rang De":"Spotify/Songs/song105.mp3",
            
            
        
            "1 Sunn Raha Hai":"Spotify/Songs/song106.mp3",
            "2 Samjhawan":"Spotify/Songs/song107.mp3",
            "3 Saibo":"Spotify/Songs/song108.mp3",
            "4 Saans":"Spotify/Songs/song109.mp3",
            "5 Kasto Mazza":"Spotify/Songs/song110.mp3",
            "6 Tu":"Spotify/Songs/song111.mp3",
            "7 Tum Kya Mile":"Spotify/Songs/song112.mp3",
            "8 Jugraafiya":"Spotify/Songs/song113.mp3",
            "9 Jab Saiyaan":"Spotify/Songs/song114.mp3",
            "10 Yeh Aaina":"Spotify/Songs/song115.mp3",
            "11 Bolo Na":"Spotify/Songs/song116.mp3",
            "12 Khyaal Rakhya Kar":"Spotify/Songs/song117.mp3",
            "13 Noor E Khuda":"Spotify/Songs/song118.mp3",
            "14 Aashiyan":"Spotify/Songs/song119.mp3",
            "15 Chikni Chameli":"Spotify/Songs/song120.mp3"
             
        }
        return song_paths.get(song, "")

    
        
    def create_time_slider(self):
        
        time_slider_frame = tk.Frame(self.master, bg="black", bd=1, relief=tk.RIDGE)
        time_slider_frame.place(x=50, y=700, height=70, width=1470)
         
        play_image = Image.open("Spotify/icons/Play.png")
        play_image = play_image.resize((30, 30))
        self.play_photo = ImageTk.PhotoImage(play_image)

        pause_image = Image.open("Spotify/icons/Pause.png")
        pause_image = pause_image.resize((30, 30))
        self.pause_photo = ImageTk.PhotoImage(pause_image)
        
        previous_image = Image.open("Spotify/icons/Previous.png")
        previous_image = previous_image.resize((30, 30))
        previous_photo = ImageTk.PhotoImage(previous_image)
    
        
        next_image = Image.open("Spotify/icons/Next.png")
        next_image = next_image.resize((30, 30))
        next_photo = ImageTk.PhotoImage(next_image)
        
        previous_button = tk.Button(time_slider_frame, image=previous_photo, bg="black", bd=0, command=self.previous_music)
        previous_button.image = previous_photo
        previous_button.grid(row=2, column=4, padx=(645,5)) 
        
        style = ttk.Style()
        style.configure("TimeLabel.TLabel",
                        foreground="white",  
                        background="black",  
                        font=("Arial", 12))  

        self.music_time_label = ttk.Label(time_slider_frame, text="0:00", style="TimeLabel.TLabel")
        self.music_time_label.grid(row=2, column=6, padx=(135,15)) 
        
        
        next_button = tk.Button(time_slider_frame, image=next_photo, bg="black", bd=0, command=self.play_next_song)
        next_button.image = next_photo
        next_button.grid(row=2, column=5, padx=(115,5)) 
        
        
        self.play_button = tk.Button(time_slider_frame, image=self.play_photo, bd=0, bg="black", command=self.toggle_play_pause)
        self.play_button.place(x=723, y=2)
        
        style = ttk.Style()
        style.configure("Volume.Horizontal.TScale",
                    background="black",  
                    troughcolor="gray", 
                    sliderthickness=1,  
                    sliderlength=1)    
        self.volume_slider = ttk.Scale(time_slider_frame, from_=0, to=100, orient="horizontal", length=100, command=self.update_volume,style="Volume.Horizontal.TScale")
        self.volume_slider.set(50)
        self.volume_slider.grid(row=2, column=6, padx=(15, 115)) 
    
        
        global pbar
        pbar = ttk.Progressbar(self.master, orient="horizontal", length=680, mode="determinate",style="white.Horizontal.TProgressbar")
        pbar.place(x=450, y=735)
        
        
        pbar["maximum"] = 400
        pbar["value"] = 0

        threading.Thread(target=self.update_progress).start()
        
    def update_music_time(self):
        if pygame.mixer.music.get_busy():
           
            current_time_ms = pygame.mixer.music.get_pos()

            
            minutes = current_time_ms // 60000
            seconds = (current_time_ms % 60000) // 1000
            self.music_time_label.config(text=f"{minutes}:{seconds:02}")

       
        self.music_time_label.after(1000, self.update_music_time)
        
        
    def toggle_play_pause(self):
        
        if self.play_button["image"] == str(self.play_photo):
            self.play_button.configure(image=self.pause_photo)
            self.play_music()
        else:
            self.play_button.configure(image=self.play_photo)
            self.pause_music()
            
    def update_time_slider_thread(self, time_label):
        while pygame.mixer.music.get_busy():
            time.sleep(0.1)
            current_position = pygame.mixer.music.get_pos() / 1000
            pbar["value"] = current_position
            if current_position >= pbar["maximum"]:
                self.stop_music()
                pbar["value"] = 0
                break

            
            minutes, seconds = divmod(int(current_position), 60)
            time_string = "{:02d}:{:02d}".format(minutes, seconds)
            time_label.config(text=time_string) 

    def update_progress(self):
            
        while True:
            if pygame.mixer.music.get_busy() and not paused:
                current_position = pygame.mixer.music.get_pos() / 1000
                pbar["value"] = current_position

                
                if current_position >= pbar["maximum"]:
                    self.stop_music() 
                    pbar["value"] = 0 
                    
                self.master.update()
            time.sleep(0.1)
    
    
    def play_next_song(self):
       
        current_index = self.song_listbox.curselection()
        
        if not current_index or current_index[0] == self.song_listbox.size() - 1:
            return

        
        next_index = current_index[0] + 1
        next_song = self.song_listbox.get(next_index)
        next_song_path = self.get_song_path(next_song)


       
        self.play_song(next_song_path)
        self.display_song_image(next_song)
        threading.Thread(target=self.update_time_slider_thread).start()
        
        self.song_listbox.selection_clear(0, tk.END)
        self.song_listbox.selection_set(next_index)

    def previous_music(self):
        
        current_index = self.song_listbox.curselection()

       
        if not current_index or current_index[0] == 0:
            return

        
        prev_index = current_index[0] - 1
        prev_song = self.song_listbox.get(prev_index)
        prev_song_path = self.get_song_path(prev_song)

        
        self.play_song(prev_song_path)
        self.display_song_image(prev_song)
        threading.Thread(target=self.update_time_slider_thread).start()

        
        self.current_song_index = prev_index
        self.song_listbox.selection_clear(0, tk.END)
        self.song_listbox.selection_set(prev_index)
    def play_music(self):
        pygame.mixer.music.unpause()

    def pause_music(self):
        pygame.mixer.music.pause()
        self.is_playing = False

    def stop_music(self):
        pygame.mixer.music.stop()
        self.is_playing = False
        self.pbar["value"] = 0
        
    def update_progress(self):
        while True:
            if pygame.mixer.music.get_busy() and not paused:
                current_position = pygame.mixer.music.get_pos() / 1000
                pbar["value"] = current_position
                
                if current_position >= pbar["maximum"]:
                    self.stop_music() 
                    pbar["value"] = 0 
                    
                self.master.update()
            time.sleep(0.1)
                     
    def update_volume(self, event):
        volume = self.volume_slider.get() / 100
        pygame.mixer.music.set_volume(volume)
            
    def play_song(self, song_path):
        # Load and play the selected song
        pygame.mixer.music.load(song_path)
        pygame.mixer.music.play()
        self.current_song = song_path
    
    def play_video(video_file):
        clip = VideoFileClip(video_file)
        clip.preview()

    play_video("Spotify/music.mp4")
    

paused = False
    
   
def main():
    pygame.init()
    root = tk.Tk()
    app = Spotify(root)
    root.protocol("WM_DELETE_WINDOW", root.destroy)
    fig, ax = plt.subplots()
    root.mainloop()

if __name__ == "__main__":
    main()
